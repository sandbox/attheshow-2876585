<?php

/**
 * @file
 *   Views query plugin for Evanced Signup events.
 */

/**
 * Views query plugin for the Evanced Signup events.
 */
class evanced_signup_plugin_query extends views_plugin_query {

  // "First we need to override the query() method with an empty one.
  // In normal views operation this is where SQL queries are constructed,
  // and since that doesn't apply to us, we just eliminate that functionality."
  function query($get_count = FALSE) { }

  function execute(&$view) {
    //dpm($this->where, 'this->where');
    $conditions = $this->where[1]['conditions'];
    //dpm($conditions, 'conditions');
    // Setup pager
    $view->init_pager();
    if ($this->pager->use_pager()) {
      $this->pager->set_current_page($view->current_page);
    }

    $headers = array();
    $url = 'http://demozonepublic.evanced.info:80/api/signup/eventlist?';
    $params = '';
    // Filters
    if ($this->where) {
      foreach ($conditions as $filter) {
        $value = $filter['value'];
        $operator = $filter['operator'];
        switch ($filter['field']) {
          case 'start_date':
            if (is_array($value)) {
              foreach ($value as $date) {
                $dates[] = $date;
              }
              //dpm($dates, 'dates');
            }
            if ($operator == 'between') {
              $params .= '&startdate=' . $dates[0] . '&enddate=' . $dates[1];
            }
            else if ($operator == '>=') {
              $params .= '&startdate=' . $dates[0];
            }
            else if ($operator == '<=') {
              $params .= '&enddate=' . $dates[0];
            }
            else if ($operator == '=') {
              $params .= '&startdate=' . $dates[0] . '&enddate=' . $dates[0];
            }
            break;
          case '.id':
            //&eventid=5863
            $params .= '&eventid=' . urlencode($value);
            break;
          case '.keyword':
            //&keyword=Cooking%20Tips
            $params .= '&keyword=' . urlencode($value);
            break;
          case '.featured':
            //&onlyfeaturedevents=true
            if ($value == 1) {
              $params .= '&onlyfeaturedevents=true';
            }
            else {
              $params .= '&onlyfeaturedevents=false';
            }
            break;
          case '.registration':
            //&onlyregistrationenabled=true
            if ($value == 1) {
              $params .= '&onlyregistrationenabled=true';
            }
            else {
              $params .= '&onlyregistrationenabled=false';
            }
            break;
          case '.location_id':
            //&locations=1004
            $params .= '&locations=' . $value;
            break;
          case '.locations_filter': // Multi select filter version.
            if (!is_array($value) && is_numeric($value)) {
              $params .= '&locations=' . $value;
            }
            else if (is_array($value)) {
              foreach ($value as $type) {
                $params .= '&locations=' . $type;
              }
            }
            break;
          case '.event_types': // Basic numeric filter
            // &eventstypes=19
            $params .= '&eventstypes=' . $value;
            break;
          case '.event_types_filter': // Multi select filter version.
            if (!is_array($value) && is_numeric($value)) {
              $params .= '&eventstypes=' . $value;
            }
            else if (is_array($value)) {
              foreach ($value as $type) {
                $params .= '&eventstypes=' . $type;
              }
            }
            break;
          case '.age_groups':
            // &agegroups=7
            $params .= '&agegroups=' . $value;
            break;
          case '.age_groups_filter': // Multi select filter version.
            if (!is_array($value) && is_numeric($value)) {
              $params .= '&agegroups=' . $value;
            }
            else if (is_array($value)) {
              foreach ($value as $type) {
                $params .= '&agegroups=' . $type;
              }
            }
            break;
          default:
            # code...
            break;
        }
      }
    }
    if (empty($params)) {
      $params .= '1=1'; // eventlist fails if no querystring is passed.
    }
    dpm($url . $params, 'url + params');
    // Sorting... (SORTING CURRENTLY NOT POSSIBLE via SignUp API)
    /*if ($this->orderby) {
      foreach ($this->orderby as $orderby) {
        switch ($orderby['field']) {
          case 'start_date':
            # code...
            break;
          default:
            # code...
            break;
        }
      }
    }*/

    // Request data from Evanced and cache it.
    $cached = cache_get('evanced_signup:' . $params, 'cache'); // Check cache.
    if ($cached && time() < $cached->expire && !empty($cached->data)) {
      $event_list = drupal_json_decode($cached->data);
    }
    else {
      $results = drupal_http_request($url . $params, $headers, 'GET');
      cache_set('evanced_signup:' . $params, $results->data, 'cache', time() + 10800); // Cache the event list for 3 hours.
      $event_list = drupal_json_decode($results->data);
    }

    // Pager totals.
    $this->pager->total_items = count($event_list);
    $this->pager->update_page_info();
    $current_page = $this->pager->current_page;
    $num_results_per_page = $this->pager->options['items_per_page'];
    $min_result = $current_page * $num_results_per_page;
    $max_result = $current_page * $num_results_per_page + $num_results_per_page - 1;

    // Go through the event list
    foreach ($event_list as $key => $event) {
      if (($key >= $min_result && $key <= $max_result) || empty($current_page)) { // Accounting for the pager.
        $row = new stdClass;
        // Fill in all of the fields we created w/ data from the API result.
        $row->id = $event['EventId'];
        $row->title = $event['Title'];
        $row->image = $event['Image'];
        $row->start_date = strtotime($event['EventStart']);
        $row->featured = $event['FeatureEvent'];
        $row->registration = ($event['RegistrationTypeCodeEnum'] == 0) ? FALSE : TRUE;
        $row->location_id = $event['LocationId'];
        $row->event_types = $event['EventTypes'];
        $row->age_groups = $event['AgeGroups'];
        $row->description = $event['Description'];
        $row->last_updated = strtotime($event['LastUpdated']);
        $row->date_entered = strtotime($event['DateEntered']);
        $row->ongoing_start_date = strtotime($event['OnGoingStartDate']);
        $row->ongoing_end_date = strtotime($event['OnGoingEndDate']);
        $row->registration_type = $event['RegistrationTypeCodeValue'];
        $row->presenter = $event['Presenter'];
        $row->image_alt = $event['ImageAlt'];
        $row->link_text = $event['LinkText'];
        $row->link_address = $event['LinkAddress'];
        $row->contact_name = $event['ContactName'];
        $row->contact_email = $event['ContactEmail'];
        $row->contact_phone = $event['ContactPhone'];
        $row->patron_status_display = $event['PatronStatusDisplay'];
        $row->location_name = $event['LocationName'];
        $row->space_name = $event['SpaceName'];
        $row->event_types_string = $event['EventTypesString'];
        $row->age_groups_string = $event['AgeGroupsString'];
        $row->tags_string = $event['TagsString'];
        $row->color_code_string = $event['ColorCodeString'];
        $row->event_length = $event['EventLength'];
        $row->space_id = $event['SpaceId'];
        $row->image_height = $event['ImageHeight'];
        $row->image_width = $event['ImageWidth'];
        $row->media_id = $event['MediaId'];
        $row->payment_required = $event['PaymentRequired'];
        $row->cancelled = $event['Cancelled'];
        $row->published = $event['Published'];
        $row->private = $event['Private'];
        $row->all_day = $event['AllDay'];
        $row->card_required = $event['CardRequired'];
        $row->deleted = $event['IsDeleted'];
        $row->reservation = $event['IsReservation'];
        //dpm($row, 'row');
        //dpm($event, 'event');

        // Push all of the row details into the view result.
        $view->result[] = $row;
      }
    }
  }

  // Empty add_field function. Not necessary except for error prevention.
  function add_field($table, $field, $alias = '', $params = array()) { }

  /**
   * Add SORT attribute to the query.
   *
   * @param $table
   *   NULL, don't use this.
   * @param $field
   *   The metric/dimensions/field
   * @param $order
   *   Either '' for ascending or '-' for descending.
   * @param $alias
   *   Don't use this yet (at all?).
   * @param $params
   *   Don't use this yet (at all?).
   */
  function add_orderby($table, $field = NULL, $order = 'ASC', $alias = '', $params = array()) {
    $this->orderby[] = array(
      'field' => $field,
      'direction' => $order,
    );
  }

  /**
   * Provides a unique placeholders for handlers.
   */
  function placeholder($base = 'views') {
    static $placeholders = array();
    if (!isset($placeholders[$base])) {
      $placeholders[$base] = 0;
      return ':' . $base;
    }
    else {
      return ':' . $base . ++$placeholders[$base];
    }
  }

  /**
   * Add a complex WHERE clause to the query.
   *
   * The caller is responsible for ensuring that all fields are fully qualified
   * (TABLE.FIELD) and that the table already exists in the query.
   * Internally the dbtng method "where" is used.
   *
   * @param $group
   *   The WHERE group to add these to; groups are used to create AND/OR
   *   sections. Groups cannot be nested. Use 0 as the default group.
   *   If the group does not yet exist it will be created as an AND group.
   * @param $snippet
   *   The snippet to check. This can be either a column or
   *   a complex expression like "UPPER(table.field) = 'value'"
   * @param $args
   *   An associative array of arguments.
   *
   * @see QueryConditionInterface::where()
   */
  function add_where_expression($group, $snippet, $args = array()) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->set_where_group('AND', $group);
    }

    $field = $snippet;
    $operator = '=';
    // Date filtering
    if (strstr($snippet, 'DATE_FORMAT')) {
      //dpm($snippet, 'snippet');
      preg_match('/DATE_FORMAT\(ADDTIME\(FROM_UNIXTIME\(\.([^\)]*)\), SEC_TO_TIME\(([^\)]*)\)\), .*\s([^\s]*)\s/u', $snippet, $matches);
      //dpm($matches, 'matches');
      $field = $matches[1];
      if (strstr($snippet, '>=') && strstr($snippet, '<=')) {
        $operator = 'between';
      }
      else {
        $operator = $matches[3];
      }
    }

    $this->where[$group]['conditions'][] = array(
      'field' => $field,
      'value' => $args,
      'operator' => $operator,
    );
  }

  // Just a dupe of the basic add_where() function...
  function add_where($group, $field, $value = NULL, $operator = NULL) {
    if (empty($group)) {
      $group = 0;
    }
    if (!isset($this->where[$group])) {
      $this->set_where_group('AND', $group);
    }
    $this->where[$group]['conditions'][] = array(
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    );
  }

  // Prevents AJAX error in Views when this method isn't present.
  function ensure_table($table, $relationship = NULL, $join = NULL) {
    return;
  }

}
