<?php

/**
 * @file
 * Provides administrator related functions for the module.
 */

/**
 * Admin settings form
 */
function evanced_signup_settings_form() {
  $form = array();

  $form['evanced_signup_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Evanced Signup API Key'),
    '#description' => t('Enter the API key assigned to you by Evanced for use with Signup.'),
    '#default_value' => variable_get('evanced_signup_api_key'),
    '#size' => 50,
    '#maxlength' => 40,
    '#required' => FALSE,
  );
  $form['evanced_signup_api_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Evanced Signup API Host'),
    '#description' => t('Enter the Evanced <b>API Host</b> for the Signup API (e.g., catalog.yourlibrary.com)'),
    '#default_value' => variable_get('evanced_signup_api_host'),
    '#size' => 80,
    '#maxlength' => 80,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
