<?php

/**
 * Implementation of hook_views_plugins().
 *
 * "The first thing we need to do is describe our plugin to views."
 */
function evanced_signup_views_plugins() {
  $plugin = array();
  $plugin['query']['evanced_signup_plugin_query'] = array(
    'title' => t('Evanced Signup Query'),
    'help' => t('Evanced Signup query object.'),
    'handler' => 'evanced_signup_plugin_query',
  );
  return $plugin;
}

/**
 * Implementation of hook_views_data().
 *
 * "Usually hook_views_data() is used to describe the tables that a module is
 * making available to Views. However in the case of a query plugin it is used
 * to describe the data provided by the external service."
 */
function evanced_signup_views_data() {
  $data = array();

  // Base data
  $data['evanced_signup']['table']['group']  = t('Evanced Signup');
  $data['evanced_signup']['table']['base'] = array(
    'title' => t('Evanced Signup Events'),
    'help' => t('Query Evanced Signup.'),
    'query class' => 'evanced_signup_plugin_query'
  );

  // String Text Fields
  $data['evanced_signup']['title'] = array(
    'title' => t('Title'),
    'help' => t('The title of this event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['registration_type'] = array(
    'title' => t('Registration type'),
    'help' => t('The type of registration allowed for the event (Noregistration, etc.).'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['presenter'] = array(
    'title' => t('Presenter'),
    'help' => t('The person or organization presenting the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['image_alt'] = array(
    'title' => t('Image alt text'),
    'help' => t('The alternate text that accompanies the image for the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['link_text'] = array(
    'title' => t('Link text'),
    'help' => t('The text for the event link.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['contact_name'] = array(
    'title' => t('Contact name'),
    'help' => t('The person to be contacted for more information.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['contact_email'] = array(
    'title' => t('Contact email'),
    'help' => t('The email address of the contact person.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['contact_phone'] = array(
    'title' => t('Contact phone'),
    'help' => t('The phone number to call for more information about the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['patron_status_display'] = array(
    'title' => t('Patron status display'),
    'help' => t('Patron status display'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['location_name'] = array(
    'title' => t('Location name'),
    'help' => t('The proper name of the location where the event is taking place.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['space_name'] = array(
    'title' => t('Space name'),
    'help' => t('The name of the space where the event is being held.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['event_types_string'] = array(
    'title' => t('Event types string'),
    'help' => t('The event types which have been associated with the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['age_groups_string'] = array(
    'title' => t('Age groups string'),
    'help' => t('A listing of which age groups the event is catered towards.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['tags_string'] = array(
    'title' => t('Tags'),
    'help' => t('The tags associated with the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['color_code_string'] = array(
    'title' => t('Color code'),
    'help' => t('The hexadecimal color CSS for the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field',
      'click sortable' => TRUE,
    ),
  );

  // Numeric fields
  $data['evanced_signup']['id'] = array(
    'title' => t('Event ID'),
    'help' => t('The ID number of this event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'evanced_signup_filter_handler_equal_only_numeric',
    ),
  );
  $data['evanced_signup']['location_id'] = array(
    'title' => t('Location ID'),
    'help' => t('The ID number of this location.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'evanced_signup_filter_handler_equal_only_numeric',
    ),
  );
  $data['evanced_signup']['event_length'] = array(
    'title' => t('Event length'),
    'help' => t('The length of time (in minutes) for the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['space_id'] = array(
    'title' => t('Space ID'),
    'help' => t('The ID number of the space being used for the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['image_height'] = array(
    'title' => t('Image height'),
    'help' => t('The pixel height for the image that accompanies the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['image_width'] = array(
    'title' => t('Image width'),
    'help' => t('The pixel width for the image that accompanies the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['media_id'] = array(
    'title' => t('Media ID'),
    'help' => t('Media ID'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );

  // Image fields
  $data['evanced_signup']['image'] = array(
    'title' => t('Image'),
    'help' => t('The actual image from Evanced.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_image',
    ),
  );

  // Date fields
  $data['evanced_signup']['start_date'] = array(
    'title' => t('Start date'),
    'help' => t('The date that the event starts on.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'evanced_signup_filter_handler_date',
    ),
    // (SORTING CURRENTLY NOT POSSIBLE via SignUp API)
    /*'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),*/
  );
  $data['evanced_signup']['last_updated'] = array(
    'title' => t('Last Updated'),
    'help' => t('The date that the event was last updated in Evanced.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['date_entered'] = array(
    'title' => t('Date Entered'),
    'help' => t('The date that the event was created in Evanced.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['ongoing_start_date'] = array(
    'title' => t('Ongoing Start Date'),
    'help' => t('The date that an ongoing event at the library begins.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['ongoing_end_date'] = array(
    'title' => t('Ongoing End Date'),
    'help' => t('The date that an ongoing event at the library ends.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_date',
      'click sortable' => TRUE,
    ),
  );

  // Boolean fields
  $data['evanced_signup']['featured'] = array(
    'title' => t('Featured event'),
    'help' => t('Whether or not the event is flagged as featured.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'use equal' => TRUE, // Use status = 1 instead of status <> 0 in WHERE statment
    ),
  );
  $data['evanced_signup']['registration'] = array(
    'title' => t('Registration enabled'),
    'help' => t('Whether or not the customers can register to attend.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'use equal' => TRUE, // Use status = 1 instead of status <> 0 in WHERE statment
    ),
  );
  $data['evanced_signup']['payment_required'] = array(
    'title' => t('Payment required'),
    'help' => t('Whether or not customers must pay to attend the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['cancelled'] = array(
    'title' => t('Cancelled'),
    'help' => t('Whether or not the event has been cancelled.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['published'] = array(
    'title' => t('Published'),
    'help' => t('Whether or not the event has been published.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['private'] = array(
    'title' => t('Private'),
    'help' => t('Whether or not the event is private.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['all_day'] = array(
    'title' => t('All day'),
    'help' => t('Whether or not the event is an all day event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['card_required'] = array(
    'title' => t('Card required'),
    'help' => t('Whether or not a library card is required to attend.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['deleted'] = array(
    'title' => t('Deleted'),
    'help' => t('Whether or not the event has been deleted.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
  );
  $data['evanced_signup']['reservation'] = array(
    'title' => t('Reservation'),
    'help' => t('Is it a reservation?'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_boolean',
      'click sortable' => TRUE,
    ),
  );

  // List fields
  $data['evanced_signup']['event_types'] = array(
    'title' => t('Event Types (Using Numeric ID)'),
    'help' => t('The ID numbers of the event types.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_prerender_list',
    ),
    'filter' => array(
      'handler' => 'evanced_signup_filter_handler_equal_only_numeric',
    ),
  );
  $data['evanced_signup']['age_groups'] = array(
    'title' => t('Age Groups (Using Numeric ID)'),
    'help' => t('The ID numbers of the age groups.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_prerender_list',
    ),
    'filter' => array(
      'handler' => 'evanced_signup_filter_handler_equal_only_numeric',
    ),
  );

  // Markup Fields
  $data['evanced_signup']['description'] = array(
    'title' => t('Description'),
    'help' => t('A short informational paragraph about the event.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_markup',
      'format' => filter_default_format(),
    ),
  );

  // URL fields
  $data['evanced_signup']['link_address'] = array(
    'title' => t('Link address'),
    'help' => t('The address for the event hyperlink.'),
    'field' => array(
      'handler' => 'evanced_signup_handler_field_url',
    ),
  );

  /*
   * FILTERS ONLY (No corresponding field)
   */
  $data['evanced_signup']['keyword'] = array(
    'title' => t('Keyword'),
    'help' => t('A keyword for filtering the list of events'),
    'filter' => array(
      'handler' => 'evanced_signup_filter_handler_equal_only_string',
    ),
  );
  $data['evanced_signup']['event_types_filter'] = array(
    'title' => t('Event Types'),
    'help' => t('Filter the listed event types.'),
    'filter' => array(
      'handler' => 'evanced_signup_filter_handler_event_types',
    ),
  );
  $data['evanced_signup']['age_groups_filter'] = array(
    'title' => t('Age Groups'),
    'help' => t('Filter the listed age groups.'),
    'filter' => array(
      'handler' => 'evanced_signup_filter_handler_age_groups',
    ),
  );
  $data['evanced_signup']['locations_filter'] = array(
    'title' => t('Locations'),
    'help' => t('Filter the listed locations.'),
    'filter' => array(
      'handler' => 'evanced_signup_filter_handler_locations',
    ),
  );

  return $data;
}
