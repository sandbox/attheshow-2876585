<?php

/**
 * Implements hook_views_default_views().
 */
function evanced_signup_views_default_views() {

  // Exported view goes here
  $view = new view();
  $view->name = 'evanced_signup_events';
  $view->description = 'Shows events via the Evanced SignUp API.';
  $view->tag = 'default';
  $view->base_table = 'evanced_signup';
  $view->human_name = 'Evanced Signup Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Events';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'id' => 'id',
    'image' => 'image',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'image' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Evanced Signup: Event ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'evanced_signup';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Field: Evanced Signup: Start date */
  $handler->display->display_options['fields']['start_date']['id'] = 'start_date';
  $handler->display->display_options['fields']['start_date']['table'] = 'evanced_signup';
  $handler->display->display_options['fields']['start_date']['field'] = 'start_date';
  $handler->display->display_options['fields']['start_date']['date_format'] = 'long';
  $handler->display->display_options['fields']['start_date']['second_date_format'] = 'long';
  /* Field: Evanced Signup: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'evanced_signup';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  /* Field: Evanced Signup: Image */
  $handler->display->display_options['fields']['image']['id'] = 'image';
  $handler->display->display_options['fields']['image']['table'] = 'evanced_signup';
  $handler->display->display_options['fields']['image']['field'] = 'image';
  $handler->display->display_options['fields']['image']['image_style'] = 'thumbnail';
  /* Field: Evanced Signup: Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'evanced_signup';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  /* Filter criterion: Evanced Signup: Age Groups */
  $handler->display->display_options['filters']['age_groups_filter']['id'] = 'age_groups_filter';
  $handler->display->display_options['filters']['age_groups_filter']['table'] = 'evanced_signup';
  $handler->display->display_options['filters']['age_groups_filter']['field'] = 'age_groups_filter';
  $handler->display->display_options['filters']['age_groups_filter']['value'] = array(
    5 => '5',
  );
  $handler->display->display_options['filters']['age_groups_filter']['exposed'] = TRUE;
  $handler->display->display_options['filters']['age_groups_filter']['expose']['operator_id'] = 'age_groups_filter_op';
  $handler->display->display_options['filters']['age_groups_filter']['expose']['label'] = 'Age Groups';
  $handler->display->display_options['filters']['age_groups_filter']['expose']['operator'] = 'age_groups_filter_op';
  $handler->display->display_options['filters']['age_groups_filter']['expose']['identifier'] = 'age_groups_filter';
  $handler->display->display_options['filters']['age_groups_filter']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    10 => 0,
    21 => 0,
    13 => 0,
    9 => 0,
    24 => 0,
    15 => 0,
    14 => 0,
    17 => 0,
    16 => 0,
    20 => 0,
  );
  /* Filter criterion: Evanced Signup: Event Types */
  $handler->display->display_options['filters']['event_types_filter']['id'] = 'event_types_filter';
  $handler->display->display_options['filters']['event_types_filter']['table'] = 'evanced_signup';
  $handler->display->display_options['filters']['event_types_filter']['field'] = 'event_types_filter';
  $handler->display->display_options['filters']['event_types_filter']['value'] = array(
    1026 => '1026',
    1027 => '1027',
    10 => '10',
    1024 => '1024',
    24 => '24',
    1025 => '1025',
    8 => '8',
    23 => '23',
    20 => '20',
    19 => '19',
    16 => '16',
    14 => '14',
    6 => '6',
    22 => '22',
    9 => '9',
    11 => '11',
  );
  $handler->display->display_options['filters']['event_types_filter']['exposed'] = TRUE;
  $handler->display->display_options['filters']['event_types_filter']['expose']['operator_id'] = 'event_types_filter_op';
  $handler->display->display_options['filters']['event_types_filter']['expose']['label'] = 'Event Types';
  $handler->display->display_options['filters']['event_types_filter']['expose']['operator'] = 'event_types_filter_op';
  $handler->display->display_options['filters']['event_types_filter']['expose']['identifier'] = 'event_types_filter';
  $handler->display->display_options['filters']['event_types_filter']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['event_types_filter']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    10 => 0,
    21 => 0,
    13 => 0,
    9 => 0,
    24 => 0,
    15 => 0,
    14 => 0,
    17 => 0,
    16 => 0,
    20 => 0,
  );
  /* Filter criterion: Evanced Signup: Featured event */
  $handler->display->display_options['filters']['featured']['id'] = 'featured';
  $handler->display->display_options['filters']['featured']['table'] = 'evanced_signup';
  $handler->display->display_options['filters']['featured']['field'] = 'featured';
  $handler->display->display_options['filters']['featured']['value'] = '0';
  /* Filter criterion: Evanced Signup: Keyword */
  $handler->display->display_options['filters']['keyword']['id'] = 'keyword';
  $handler->display->display_options['filters']['keyword']['table'] = 'evanced_signup';
  $handler->display->display_options['filters']['keyword']['field'] = 'keyword';
  $handler->display->display_options['filters']['keyword']['exposed'] = TRUE;
  $handler->display->display_options['filters']['keyword']['expose']['operator_id'] = 'keyword_op';
  $handler->display->display_options['filters']['keyword']['expose']['label'] = 'Keyword';
  $handler->display->display_options['filters']['keyword']['expose']['operator'] = 'keyword_op';
  $handler->display->display_options['filters']['keyword']['expose']['identifier'] = 'keyword';
  $handler->display->display_options['filters']['keyword']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    10 => 0,
    21 => 0,
    13 => 0,
    9 => 0,
    24 => 0,
    15 => 0,
    14 => 0,
    17 => 0,
    16 => 0,
    20 => 0,
  );
  /* Filter criterion: Evanced Signup: Locations */
  $handler->display->display_options['filters']['locations_filter']['id'] = 'locations_filter';
  $handler->display->display_options['filters']['locations_filter']['table'] = 'evanced_signup';
  $handler->display->display_options['filters']['locations_filter']['field'] = 'locations_filter';
  $handler->display->display_options['filters']['locations_filter']['value'] = array(
    0 => '0',
    1004 => '1004',
  );
  $handler->display->display_options['filters']['locations_filter']['exposed'] = TRUE;
  $handler->display->display_options['filters']['locations_filter']['expose']['operator_id'] = 'locations_filter_op';
  $handler->display->display_options['filters']['locations_filter']['expose']['label'] = 'Locations';
  $handler->display->display_options['filters']['locations_filter']['expose']['operator'] = 'locations_filter_op';
  $handler->display->display_options['filters']['locations_filter']['expose']['identifier'] = 'locations_filter';
  $handler->display->display_options['filters']['locations_filter']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['locations_filter']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    10 => 0,
    21 => 0,
    13 => 0,
    9 => 0,
    24 => 0,
    15 => 0,
    14 => 0,
    17 => 0,
    16 => 0,
    20 => 0,
  );
  /* Filter criterion: Evanced Signup: Start date */
  $handler->display->display_options['filters']['start_date']['id'] = 'start_date';
  $handler->display->display_options['filters']['start_date']['table'] = 'evanced_signup';
  $handler->display->display_options['filters']['start_date']['field'] = 'start_date';
  $handler->display->display_options['filters']['start_date']['operator'] = 'between';
  $handler->display->display_options['filters']['start_date']['default_date'] = 'today';
  $handler->display->display_options['filters']['start_date']['default_to_date'] = '+1 month';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'events';

  // Not part of the export, but we still need to return.
  $views[$view->name] = $view;
  return $views;
}
