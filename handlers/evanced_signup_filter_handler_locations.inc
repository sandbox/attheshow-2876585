<?php

class evanced_signup_filter_handler_locations extends views_handler_filter_many_to_one {

  function get_value_options() {

    // Query the Signup API to pull in the list of event types.
    $headers = array();
    $url = 'http://demozonepublic.evanced.info:80/api/signup/locations';
    // Request data from Evanced and cache it.
    $cached = cache_get('evanced_signup_locations', 'cache'); // Check cache.
    if ($cached && time() < $cached->expire && !empty($cached->data)) {
      $locations_array = drupal_json_decode($cached->data);
    }
    else {
      $results = drupal_http_request($url, $headers, 'GET');
      cache_set('evanced_signup_locations', $results->data, 'cache', time() + 10800); // Cache the event list for 3 hours.
      $locations_array = drupal_json_decode($results->data);
    }
    // Push them into an associative array w/ the ids and the names.
    $locations = array();
    foreach ($locations_array as $type) {
      $locations[$type['LocationId']] = $type['Name'];
    }
    // These become the options.
    $this->value_options = $locations;
  }

  /**
   * Remove a couple of unusable operators
   */
  function operators() {
    $operators = parent::operators();
    unset($operators['and'], $operators['not']); // Signup API doesn't allow for these.
    return $operators;
  }

}
