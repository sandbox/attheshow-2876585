<?php
class evanced_signup_filter_handler_date extends date_views_filter_handler_simple {
  function operators() {
    $operators = parent::operators();
    // Remove some of the date operators that don't work on the SignUp api
    unset($operators['<'], $operators['>'], $operators['!='], $operators['not between'], $operators['regular_expression'], $operators['contains']);
    return $operators;
  }
}
