<?php

/**
 * @file
 *   Views field handler for basic Evanced Signup date fields.
 *
 * The only thing we're doing here is making sure the field_alias
 * gets set properly, and that none of the sql-specific query functionality
 * gets called.
 */
class evanced_signup_handler_field_date extends views_handler_field_date {
  function query() {
    $this->field_alias = $this->real_field;
  }
}
