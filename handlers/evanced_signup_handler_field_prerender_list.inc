<?php

class evanced_signup_handler_field_prerender_list extends views_handler_field_prerender_list {

  /**
   * Render the field.
   */
  function render($values) {
    // Fixed rendering. Doesn't work w/ normal parameters for some reason on these fields... whatevs.
    $field = $this->options['field'];
    // Remove 0 values. Corresponds to empty age group or empty event type from Evanced.
    $values_cleaned = array();
    foreach ($values->$field as $value) {
      if ($value != 0) {
        $values_cleaned[] = $value;
      }
    }
    if (!empty($values->$field)) {
      if ($this->options['type'] == 'separator') {
        return implode($this->sanitize_value($this->options['separator']), $values_cleaned);
      }
      else {
        return theme('item_list',
          array(
            'items' => $values_cleaned,
            'title' => NULL,
            'type' => $this->options['type']
          ));
      }
    }
  }

}
