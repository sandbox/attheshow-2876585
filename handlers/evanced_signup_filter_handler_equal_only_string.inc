<?php
class evanced_signup_filter_handler_equal_only_string extends views_handler_filter_string {
  function operators() {
    $operators = parent::operators();
    // Remove some of the date operators that don't work on the SignUp api
    foreach ($operators as $key => $value) {
      if ($key != '=') {
        unset($operators[$key]);
      }
    }
    return $operators;
  }
}
