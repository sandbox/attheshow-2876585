<?php

class evanced_signup_filter_handler_age_groups extends views_handler_filter_many_to_one {

  function get_value_options() {

    // Query the Signup API to pull in the list of event types.
    $headers = array();
    $url = 'http://demozonepublic.evanced.info:80/api/signup/agegroups';
    // Request data from Evanced and cache it.
    $cached = cache_get('evanced_signup_agegroups', 'cache'); // Check cache.
    if ($cached && time() < $cached->expire && !empty($cached->data)) {
      $age_groups_array = drupal_json_decode($cached->data);
    }
    else {
      $results = drupal_http_request($url, $headers, 'GET');
      cache_set('evanced_signup_agegroups', $results->data, 'cache', time() + 10800); // Cache the event list for 3 hours.
      $age_groups_array = drupal_json_decode($results->data);
    }
    // Push them into an associative array w/ the ids and the names.
    $age_groups = array();
    foreach ($age_groups_array as $type) {
      $age_groups[$type['Id']] = $type['Name'];
    }
    // These become the options.
    $this->value_options = $age_groups;
  }

  /**
   * Remove a couple of unusable operators
   */
  function operators() {
    $operators = parent::operators();
    unset($operators['and'], $operators['not']); // Signup API doesn't allow for these.
    return $operators;
  }

}
